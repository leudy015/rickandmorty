/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {ApolloProvider} from 'react-apollo';
import {ApolloClient} from 'apollo-client';
import {createHttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import NavigationService from './src/services/Navigration';
import {AuthContainer} from './src/Navigation';
import {NETWORK_INTERFACE_URL} from './src/Config/urlConfig';
import NetInfo from '@react-native-community/netinfo';
import {store, persistor} from './src/redux/store';
import {ColorSchemeProvider} from 'react-native-dynamic';
import NoInterner from './src/Screen/NoInternet';

// Subscribe
const unsubscribe = NetInfo.addEventListener((state) => {
  console.log('Connection type', state.type);
  console.log('Is connected?', state.isConnected);
});
// Unsubscribe
unsubscribe();

const httpLink = createHttpLink({
  uri: NETWORK_INTERFACE_URL,
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});

const App = () => {
  const [Isconected, setIsconected] = useState(false);

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      setIsconected(state.isConnected);
    });
  }, [Isconected]);

  if (Isconected) {
    return (
      <ColorSchemeProvider>
        <Provider store={store}>
          <PersistGate loading={true} persistor={persistor}>
            <StatusBar translucent backgroundColor="transparent" />
            <ApolloProvider client={apolloClient}>
              <AuthContainer
                ref={(navigatorRef) => {
                  NavigationService.setContainer(navigatorRef);
                }}
              />
            </ApolloProvider>
          </PersistGate>
        </Provider>
      </ColorSchemeProvider>
    );
  } else {
    return <NoInterner />;
  }
};

export default App;
