import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

/////////Screens//////////////
import Home from '../Screen/Home';
import Details from '../Screen/Details';
import AllCharacters from '../Screen/AllCharacters';
import Search from '../Screen/search';
///////////end//////////////

const AuthAppNavigator = createStackNavigator(
  {
    Home,
    Details,
    AllCharacters,
    Search,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.DefaultTransition,
    },
  },
  //@ts-ignore
  {
    initialRouteName: Home,
  },
);

export const AuthSwitch = createSwitchNavigator({
  Auth: AuthAppNavigator,
});

export const AuthContainer = createAppContainer(AuthSwitch);
