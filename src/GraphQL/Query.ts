import gql from 'graphql-tag';

const CHARACTERS = gql`
  query characters($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        name
        id
        name
        status
        species
        type
        gender
        origin {
          id
          name
          residents {
            id
            image
            name
          }
        }
        location {
          id
          name
          created
          type
          residents {
            id
            image
            name
          }
        }
        image
        episode {
          id
          name
          air_date
          episode
          characters {
            id
            image
            name
          }
          created
        }
        created
      }
    }
  }
`;

const CHARACTER = gql`
  query character($id: ID!) {
    character(id: $id) {
      name
      id
      status
      species
      type
      gender
      origin {
        id
        name
        residents {
          id
          image
          name
        }
      }
      location {
        id
        name
        created
        type
        residents {
          id
          image
          name
        }
      }
      image
      episode {
        id
        name
        air_date
        episode
        characters {
          id
          image
          name
        }
        created
      }
      created
    }
  }
`;

export const query = {CHARACTERS, CHARACTER};
