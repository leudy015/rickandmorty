import React, {useState} from 'react';
import {View, RefreshControl, ScrollView} from 'react-native';
import {CustomText} from '../Components/CustomTetx';
import Card from '../Components/Card';
import {colors, dimensions, stylesText} from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Header from '../Components/Header';
import {useQuery} from 'react-apollo';
import {query} from '../GraphQL';
import Loading from '../Components/CardLoading';

export default function Search() {
  const [refreshing, setRefreshing] = useState(false);
  const [page, setPage] = useState(1);
  const [name, setname] = useState('');
  const [status, setstatus] = useState('');
  const [species, setspecies] = useState('');
  const [type, settype] = useState('');
  const [gender, setgender] = useState('');

  const styles = useDynamicValue(dynamicStyles);

  const {data, loading, refetch, error} = useQuery(query.CHARACTERS, {
    variables: {
      page: page,
      filter: {
        name: name,
        status: status,
        species: species,
        type: type,
        gender: gender,
      },
    },
  });

  const character = data && data.characters ? data.characters.results : [];

  const info = data && data.characters ? data.characters.info : {};

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      setPage(page + 1);
    }, 2000);
  };

  const LoadMoreRandomData = () => {
    setPage(page + 1);
  };

  return (
    <View style={styles.container}>
      <Header
        title="Search characters"
        left={true}
        Loading={loading}
        search={true}
        //@ts-ignore
        autoFocus={true}
        value={name}
        onChangeText={(value: any) => setname(value)}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.titleText,
            {marginLeft: 15, marginBottom: 20, marginTop: 50},
          ]}>
          {info.count} Result
        </CustomText>
        <View style={{marginBottom: dimensions.Height(10)}}>
          {loading ? (
            <Loading />
          ) : (
            <Card
              character={character}
              LoadMoreRandomData={LoadMoreRandomData}
              loading={loading}
              refetch={refetch}
              info={info}
              load={true}
            />
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
