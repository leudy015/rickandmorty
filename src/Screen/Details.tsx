import React from 'react';
import {
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {colors, dimensions, stylesText, image} from '../theme';
import {CustomText} from '../Components/CustomTetx';
import Navigation from '../services/Navigration';
import {useNavigationParam} from 'react-navigation-hooks';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import {useQuery} from 'react-apollo';
import {query} from '../GraphQL';
import Loading from '../Components/PlaceHolderDetails';
import moment from 'moment';
import EpisodeCard from '../Components/EspisodeCard';
import NoData from '../Components/NoData';

export default function Details() {
  const id = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);
  const {data, loading} = useQuery(query.CHARACTER, {
    variables: {id: id},
  });

  const character = data && data.character ? data.character : {};

  const locations =
    character && character.location ? character.location.residents : [];

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              source={{uri: character.image}}
              //@ts-ignore
              style={styles.imagenbg}
              resizeMode="cover"
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack()}
                style={styles.back}>
                <Icon
                  name="left"
                  type="AntDesign"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <View style={styles.nameDate}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={[styles.stickySectionText, {width: 200}]}>
                {character.name}
              </CustomText>
            </View>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View style={[styles.namecont, {marginBottom: dimensions.Height(8)}]}>
            <CustomText
              light={colors.black}
              dark={colors.white}
              style={stylesText.titleText}>
              {character.name}
            </CustomText>
            <View style={styles.status}>
              <View
                style={[
                  styles.point,
                  {
                    backgroundColor:
                      character.status === 'Alive'
                        ? colors.green
                        : colors.ERROR,
                  },
                ]}
              />
              <CustomText
                numberOfLines={1}
                style={[stylesText.mainText, {width: 200}]}
                light={colors.rgb_153}
                dark={colors.rgb_235}>
                {character.status} | {character.species}
              </CustomText>

              <CustomText
                style={[stylesText.secondaryText, {marginLeft: 'auto'}]}
                light={colors.rgb_153}
                dark={colors.rgb_235}>
                <Icon
                  name="tv"
                  type="Feather"
                  size={18}
                  color={colors.main}
                  style={{marginRight: 7, color: colors.secundary}}
                />{' '}
                {character.episode.length} Episode
              </CustomText>
            </View>
            <View style={styles.status}>
              <Icon
                name="transgender"
                type="FontAwesome"
                size={14}
                color={colors.main}
                style={{marginRight: 7, color: colors.secundary}}
              />
              <CustomText
                style={stylesText.secondaryText}
                light={colors.rgb_153}
                dark={colors.rgb_235}>
                {character.gender}
              </CustomText>
              <CustomText
                style={[stylesText.secondaryText, {marginLeft: 'auto'}]}
                light={colors.secundary}
                dark={colors.secundary}>
                {moment(character.created).format('ll')}
              </CustomText>
            </View>

            <View style={styles.resident}>
              <CustomText
                style={stylesText.titleText2}
                light={colors.black}
                dark={colors.white}>
                Location
              </CustomText>
              {locations ? (
                <View style={styles.residentCard}>
                  <CustomText
                    style={stylesText.secondaryTextBold}
                    light={colors.black}
                    dark={colors.white}>
                    {character.location.name}
                  </CustomText>
                  <CustomText
                    style={[stylesText.secondaryText, {marginTop: 5}]}
                    light={colors.rgb_153}
                    dark={colors.rgb_153}>
                    {character.location.type}
                  </CustomText>

                  <CustomText
                    style={[stylesText.secondaryText, {marginTop: 5}]}
                    light={colors.secundary}
                    dark={colors.secundary}>
                    {moment(character.location.created).format('ll')}
                  </CustomText>

                  <CustomText
                    style={[stylesText.titleText2, {marginTop: 10}]}
                    light={colors.black}
                    dark={colors.white}>
                    Residents
                  </CustomText>

                  <ScrollView
                    style={styles.residentCont}
                    horizontal={true}
                    showsVerticalScrollIndicator={false}>
                    {locations &&
                      locations.map((r, i) => {
                        return (
                          <View key={i} style={styles.re}>
                            <Image
                              source={{uri: r.image}}
                              style={styles.avatars}
                              resizeMode="cover"
                            />
                          </View>
                        );
                      })}
                  </ScrollView>
                </View>
              ) : (
                <NoData
                  menssge="There are no locations for this character"
                  images={image.Nodata}
                />
              )}
            </View>
            <View style={[styles.resident]}>
              <CustomText
                style={stylesText.titleText2}
                light={colors.black}
                dark={colors.white}>
                Episode
              </CustomText>
              <EpisodeCard data={character.episode} />
            </View>
          </View>
        </View>
      </ParallaxScrollView>
    </View>
  );
}

const window = Dimensions.get('window');
const AVATAR_SIZE = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.black),
    justifyContent: 'flex-end',
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    left: 50,
    top: -8,
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 10,
    left: 10,
    flexDirection: 'row',
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
    textAlign: 'center',
  },

  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    paddingVertical: 5,
  },

  contenedor: {
    width: dimensions.ScreenWidth,
    minHeight: dimensions.ScreenHeight,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.black),
    borderTopEndRadius: 25,
    borderTopStartRadius: 25,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderRadius: 10,
    padding: 10,
  },

  namecont: {
    margin: 20,
  },

  status: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 7,
  },

  point: {
    width: 10,
    height: 10,
    borderRadius: 100,
    marginRight: 7,
  },

  nameDate: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  resident: {
    marginTop: dimensions.Height(5),
  },

  residentCard: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 20,
    borderRadius: 10,
    width: dimensions.Width(90),
    marginTop: 30,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  residentCont: {
    flexDirection: 'row',
  },

  avatars: {
    width: 50,
    height: 50,
    borderRadius: 100,
    marginRight: -15,
  },

  re: {
    marginTop: 20,
  },
});
