import React, {useState} from 'react';
import {View, ScrollView, RefreshControl} from 'react-native';
import {useQuery} from 'react-apollo';
import {query} from '../GraphQL';
import Header from '../Components/Header';
import {colors, stylesText} from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../Components/CustomTetx';
import Card from '../Components/Card';
import Loading from '../Components/CardLoading';
import CardHorizontal from '../Components/CardHorizontal';

export default function Home() {
  const styles = useDynamicValue(dynamicStyles);
  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const {data, loading, refetch} = useQuery(query.CHARACTERS, {
    variables: {
      page: page,
    },
  });

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      setPage(page + 1);
    }, 2000);
  };

  const character = data && data.characters ? data.characters.results : [];

  const info = data && data.characters ? data.characters.info : {};

  const loadData = () => {
    setPage(page + 1);
  };
  return (
    <View style={styles.container}>
      <Header
        search={true}
        autoFocus={false}
        title="Home"
        Loading={false}
        value=""
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.titleText,
            {marginLeft: 15, marginBottom: 20, marginTop: 50},
          ]}>
          Characters Alive
        </CustomText>
        <CardHorizontal page={page} />
        <CustomText
          light={colors.black}
          dark={colors.white}
          style={[
            stylesText.titleText,
            {marginLeft: 15, marginBottom: 20, marginTop: loading ? 80 : 0},
          ]}>
          All characters
        </CustomText>

        {loading ? (
          <Loading />
        ) : (
          <Card
            character={character}
            loading={loading}
            refetch={refetch}
            info={info}
            load={true}
            LoadMoreRandomData={loadData}
          />
        )}
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
