import React, {useState} from 'react';
import {View, RefreshControl, ScrollView} from 'react-native';
import {CustomText} from '../Components/CustomTetx';
import Card from '../Components/Card';
import {colors, dimensions, stylesText} from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Header from '../Components/Header';
import {useNavigationParam} from 'react-navigation-hooks';

export default function AllCharacters() {
  const [refreshing, setRefreshing] = useState(false);
  const data = useNavigationParam('data');
  const styles = useDynamicValue(dynamicStyles);
  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };
  return (
    <View style={styles.container}>
      <Header autoFocus={false} title={data.name} left={true} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <View style={{marginBottom: dimensions.Height(10)}}>
          <CustomText
            light={colors.black}
            dark={colors.white}
            style={[
              stylesText.titleText,
              {marginLeft: 15, marginBottom: 20, marginTop: 50},
            ]}>
            {data.name}
          </CustomText>
          <Card character={data.characters} fromAll={true} load={false} />
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    backgroundColor: new DynamicValue(colors.white, colors.black),
  },
});
