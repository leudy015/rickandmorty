import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import {CustomText} from '../Components/CustomTetx';
import NoData from '../Components/NoData';
import {image, dimensions, colors} from '../theme';
import {stylesText} from '../theme/TextStyle';

const Ayuda = () => {
  const styles = useDynamicValue(dynamicStyles);
  return (
    <View style={styles.container}>
      <View>
        <NoData
          menssge="There is a problem with your internet connection, try again."
          images={image.Nodata}
        />
        <TouchableOpacity onPress={() => {}} style={styles.selecteds}>
          <CustomText
            light={colors.white}
            dark={colors.white}
            numberOfLines={1}
            style={stylesText.mainText}>
            Try again
          </CustomText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue('white', 'black'),
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  selecteds: {
    width: dimensions.Width(90),
    height: 60,
    borderRadius: 50,
    flexDirection: 'row',
    backgroundColor: colors.rgb_153,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
});

export default Ayuda;
