import {dimensions} from './dimension';

const mainText = {
  fontSize: dimensions.FontSize(20),
  fontWeight: '400',
  fontFamily: 'Helvetica',
};

const titleText2 = {
  fontSize: dimensions.FontSize(20),
  fontWeight: 'bold',
  fontFamily: 'Helvetica',
};

const titleText = {
  fontSize: dimensions.FontSize(24),
  fontWeight: 'bold',
  fontFamily: 'Helvetica',
};

const titleText200 = {
  fontSize: dimensions.FontSize(18),
  fontWeight: '100',
  fontFamily: 'Helvetica',
};

const titleTextNoBold = {
  fontSize: dimensions.FontSize(24),
  fontWeight: '400',
  fontFamily: 'Helvetica',
};

const secondaryText = {
  fontSize: dimensions.FontSize(14),
  fontWeight: '300',
  fontFamily: 'Helvetica',
};

const secondaryTextBold = {
  fontSize: dimensions.FontSize(16),
  fontWeight: 'bold',
  fontFamily: 'Helvetica',
};

const miniTitle = {
  fontSize: dimensions.FontSize(16),
  fontWeight: '300',
  fontFamily: 'Helvetica',
};

const terciaryText = {
  fontSize: dimensions.FontSize(14),
  fontWeight: '200',
  fontFamily: 'Helvetica',
};

const placeholderText = {
  fontSize: dimensions.FontSize(12),
  fontWeight: '100',
  fontFamily: 'Helvetica',
};

const h1 = {
  fontSize: dimensions.FontSize(30),
  fontWeight: 'bold',
  fontFamily: 'Helvetica',
};

export const stylesText = {
  mainText,
  secondaryText,
  terciaryText,
  placeholderText,
  h1,
  titleText,
  titleTextNoBold,
  titleText200,
  secondaryTextBold,
  titleText2,
  miniTitle,
};
