import React from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import {CustomText} from './CustomTetx';
import {colors, dimensions, stylesText} from '../theme';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Navigation from '../services/Navigration';
import {SearchBar} from 'react-native-elements';
const height = Dimensions.get('window').height;

export interface IHeader {
  title: string;
  search?: boolean;
  autoFocus?: boolean;
  left?: boolean;
  onChangeText?: any;
  Loading?: boolean;
  value: string;
}

export default function Header(props: IHeader) {
  const {title, autoFocus, search, left, onChangeText, Loading, value} = props;
  const styles = useDynamicValue(dynamicStyles);
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={[
          styles.headerCont,
          {
            marginBottom: !search ? dimensions.Height(3) : 0,
            marginTop: Platform.OS === 'android' ? dimensions.Height(5) : 0,
          },
        ]}>
        <View>
          <TouchableOpacity onPress={() => Navigation.goBack()}>
            {left ? (
              <CustomText>
                <Icon
                  name="left"
                  type="AntDesign"
                  size={24}
                  color={colors.secundary}
                />
              </CustomText>
            ) : (
              <CustomText>
                <Icon
                  name="staro"
                  type="AntDesign"
                  size={24}
                  color={colors.secundary}
                />
              </CustomText>
            )}
          </TouchableOpacity>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            width: dimensions.Width(50),
          }}>
          <CustomText
            numberOfLines={1}
            ligth={colors.black}
            dark={colors.white}
            style={[stylesText.mainText]}>
            {title}
          </CustomText>
        </View>
        <View>
          <TouchableOpacity onPress={() => Navigation.navigate('Search', 0)}>
            <Icon
              name="search1"
              type="AntDesign"
              size={24}
              color={colors.secundary}
            />
          </TouchableOpacity>
        </View>
      </View>
      {search ? (
        <SearchBar
          placeholder="Search you ..."
          onChangeText={onChangeText}
          onFocus={() => Navigation.navigate('Search', 0)}
          containerStyle={styles.search}
          inputContainerStyle={styles.inputs}
          showLoading={Loading}
          autoFocus={autoFocus}
          value={value}
        />
      ) : null}
    </SafeAreaView>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.ScreenWidth,
    height: 'auto',
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    borderRadius: 20,
  },

  headerCont: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: dimensions.Width(2),
  },

  closet: {
    color: new DynamicValue(colors.main, colors.white),
    paddingRight: 15,
    paddingLeft: height < 600 ? 15 : 0,
  },
  inputContainer: {
    width: dimensions.Width(94),
    height: 55,
    backgroundColor: new DynamicValue(colors.colorInput, colors.back_dark),
    marginLeft: dimensions.Width(3),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    borderRadius: 25,
    borderColor: new DynamicValue(colors.colorBorder, colors.back_suave_dark),
    borderWidth: 1.5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  input: {
    height: 65,
    width: height < 600 ? dimensions.Width(72) : dimensions.Width(75),
    paddingLeft: 10,
    fontSize: dimensions.FontSize(18),
    color: new DynamicValue(colors.main, colors.white),
  },

  search: {
    backgroundColor: 'transparent',
    borderTopWidth: 0,
    borderBottomWidth: 0,
    marginTop: 20,
    marginBottom: 10,
  },

  inputs: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 50,
  },
});
