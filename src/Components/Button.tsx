import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {CustomText} from './CustomTetx';

export const Button = (props: any) => {
  return (
    <View style={props.containerStyle}>
      <TouchableOpacity onPress={() => props.onPress()}>
        <CustomText style={props.titleStyle}>{props.title}</CustomText>
      </TouchableOpacity>
    </View>
  );
};
