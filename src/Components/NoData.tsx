import React from 'react';
import {Image, View} from 'react-native';
import {dimensions, colors} from '../theme';
import {CustomText} from './CustomTetx';

const NoData = (props: any) => {
  return (
    <View
      style={{alignSelf: 'center', padding: 20, width: dimensions.Width(100)}}>
      <Image
        source={props.images}
        style={{
          width: dimensions.Width(69),
          alignSelf: 'center',
          height: dimensions.Height(27),
        }}
        resizeMode="cover"
      />
      <CustomText
        light={colors.black}
        dark={colors.white}
        style={{
          textAlign: 'center',
          fontSize: dimensions.FontSize(20),
          fontWeight: '200',
        }}>
        {props.menssge}
      </CustomText>
    </View>
  );
};

export default NoData;
