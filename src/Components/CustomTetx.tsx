import React from 'react';
import {Text} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dynamic';

export const CustomText = (props: any) => {
  const dynamicStyles = new DynamicStyleSheet({
    TextView: {
      color: new DynamicValue(props.light, props.dark),
    },
  });
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <Text {...props} style={[styles.TextView, props.style]}>
      {props.children}
    </Text>
  );
};
