import React from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {CustomText} from './CustomTetx';
import {stylesText, colors, dimensions, image} from '../theme';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/Navigration';
import NoData from './NoData';

export default function EspisodeCard(props) {
  const styles = useDynamicValue(dynamicStyles);
  const {data} = props;

  const _renderItem = ({item}) => {
    return (
      <View>
        <View style={styles.residentCard}>
          <CustomText
            numberOfLines={1}
            style={[
              stylesText.secondaryTextBold,
              {width: dimensions.Width(80)},
            ]}
            light={colors.black}
            dark={colors.white}>
            {item.name}
          </CustomText>
          <CustomText
            style={[stylesText.secondaryText, {marginTop: 5}]}
            light={colors.rgb_153}
            dark={colors.rgb_153}>
            Episode: {item.episode}
          </CustomText>

          <CustomText
            style={[stylesText.secondaryText, {marginTop: 5}]}
            light={colors.secundary}
            dark={colors.secundary}>
            Air date: {item.air_date}
          </CustomText>

          <CustomText
            style={[stylesText.titleText2, {marginTop: 10}]}
            light={colors.black}
            dark={colors.white}>
            Characters
          </CustomText>

          <View style={styles.characters}>
            <Icon
              name="user"
              type="AntDesign"
              size={16}
              color={colors.main}
              style={{marginRight: 3, color: colors.secundary}}
            />
            <CustomText
              style={[stylesText.secondaryText, {marginTop: 5}]}
              light={colors.main}
              dark={colors.white}>
              More: {item.characters.length} characters
            </CustomText>

            <TouchableOpacity
              style={{marginLeft: 'auto'}}
              onPress={() =>
                Navigation.navigate('AllCharacters', {data: item})
              }>
              <CustomText
                style={[stylesText.secondaryText, {marginTop: 5}]}
                light={colors.secundary}
                dark={colors.secundary}>
                View all{' '}
                <Icon
                  name="right"
                  type="AntDesign"
                  size={16}
                  color={colors.main}
                  style={{marginRight: 7, color: colors.secundary}}
                />
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  return (
    <FlatList
      data={data ? data : []}
      renderItem={(item: any) => _renderItem(item)}
      keyExtractor={(item: any) => item.id}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <NoData
          menssge="There are no episode for this character"
          images={image.Nodata}
        />
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  residentCard: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 20,
    marginHorizontal: 15,
    marginVertical: 20,
    borderRadius: 10,
    width: dimensions.Width(85),
    marginTop: 30,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  residentCont: {
    flexDirection: 'row',
  },

  avatars: {
    width: 50,
    height: 50,
    borderRadius: 100,
    marginRight: -15,
  },

  re: {
    marginTop: 20,
  },

  characters: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
  },
});
