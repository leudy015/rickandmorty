import React from 'react';
import {View, TouchableOpacity, FlatList, Image} from 'react-native';
import {CustomText} from './CustomTetx';
import {Button} from './Button';
import {colors, dimensions, stylesText, image} from '../theme';
import Navigation from '../services/Navigration';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicValue,
} from 'react-native-dynamic';
import Icon from 'react-native-dynamic-vector-icons';
import moment from 'moment';
import NoData from './NoData';
import {useQuery} from 'react-apollo';
import {query} from '../GraphQL';
import Loading from './CardLoadingHorizontal';

export default function CardHorizontal(props) {
  const {data, loading} = useQuery(query.CHARACTERS, {
    variables: {
      page: props.page,
      filter: {
        status: 'Alive',
      },
    },
  });

  const character = data && data.characters ? data.characters.results : [];

  const styles = useDynamicValue(dynamicStyles);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.constainer}
        onPress={() => Navigation.navigate('Details', {data: item.id})}>
        <Image
          source={{uri: item.image}}
          //@ts-ignore
          style={styles.mainImage}
          resizeMode="cover"
        />
        <View style={styles.textCont}>
          <View style={styles.nameDate}>
            <CustomText
              numberOfLines={1}
              style={[stylesText.titleText2, {width: 200}]}
              light={colors.black}
              dark={colors.white}>
              {item.name}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 'auto'}]}
              light={colors.secundary}
              dark={colors.secundary}>
              {moment(item.created).format('ll')}
            </CustomText>
          </View>
          <View style={styles.status}>
            <View
              style={[
                styles.point,
                {
                  backgroundColor:
                    item.status === 'Alive' ? colors.green : colors.ERROR,
                },
              ]}
            />
            <CustomText
              numberOfLines={1}
              style={[stylesText.mainText, {width: 200}]}
              light={colors.rgb_153}
              dark={colors.rgb_235}>
              {item.status} | {item.species}
            </CustomText>
            <CustomText
              style={[stylesText.secondaryText, {marginLeft: 'auto'}]}
              light={colors.rgb_153}
              dark={colors.rgb_235}>
              <Icon
                name="tv"
                type="Feather"
                size={18}
                color={colors.main}
                style={{marginRight: 7, color: colors.secundary}}
              />{' '}
              {item.episode.length} Episode
            </CustomText>
          </View>
          <View style={styles.status}>
            <Icon
              name="transgender"
              type="FontAwesome"
              size={14}
              color={colors.main}
              style={{marginRight: 7, color: colors.secundary}}
            />
            <CustomText
              style={stylesText.secondaryText}
              light={colors.rgb_153}
              dark={colors.rgb_235}>
              {item.gender}
            </CustomText>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <FlatList
          data={character ? character : []}
          renderItem={(item: any) => _renderItem(item)}
          keyExtractor={(item: any) => item.id}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          ListEmptyComponent={
            <NoData menssge="No result for this search" images={image.Nodata} />
          }
        />
      )}
    </>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  constainer: {
    marginHorizontal: dimensions.Width(4),
    marginBottom: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
    borderRadius: 20,
  },

  mainImage: {
    width: 'auto',
    height: 150,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },

  textCont: {
    margin: dimensions.Height(2),
  },

  status: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 7,
  },

  point: {
    width: 10,
    height: 10,
    borderRadius: 100,
    marginRight: 7,
  },

  cont: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: dimensions.Height(8),
  },

  buttonView: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.Width(90),
    borderRadius: 50,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: new DynamicValue(colors.main, colors.white),
    fontWeight: '400',
    fontSize: dimensions.FontSize(17),
  },

  nameDate: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
